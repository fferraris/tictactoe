import redis, string, random, sys, json
from flask import Flask, request, abort, make_response, jsonify

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

@app.route('/startgame', methods=['POST'])
def start_game():

    game_id = ''.join(random.choice( string.ascii_letters) for i in range(20))
    gamestatus = {
        "game_id": game_id,
        'last_player': 0,
        'game_status': [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        }

    cache.set(game_id, json.dumps(gamestatus))
    return {'game_id': game_id} 


@app.route('/move', methods=['POST'])
def move():

    game_id =  request.form.get("game_id")
    player_code =  request.form.get("player_code")
    position =  request.form.get("position")

    if(game_id is None):
        abort(make_response(jsonify(error="Missing game_id"), 400))

    try:
        game_data = json.loads(cache.get(game_id))
    except TypeError:
        abort(make_response(jsonify(error="Invalid game_id"), 400))

    if game_id is None or game_data is None:
        abort(make_response(jsonify(error="Invalid game_id"), 400))

    game_status = game_data.get('game_status')

    if int(player_code) != 1 and int(player_code) != 2:
        abort(make_response(jsonify(error="Invalid player_code"), 400))

    if int(position[0]) is None or int(position[1]) is None or int(position[0]) < 0 or int(position[0]) > 2  or int(position[1]) < 0 or int(position[1]) > 2:
        abort(make_response(jsonify(error="Invalid position: out of range"), 400))

    if game_data.get('game_status')[int(position[0])][int(position[1])] != 0:
        abort(make_response(jsonify(error="Invalid position: position already taken"), 400))

    if game_data.get('last_player') != 0 and player_code == game_data.get('last_player'):
        abort(make_response(jsonify(error="Invalid player_code: not your turn"), 400))

    game_status[int(position[0])][int(position[1])] = player_code

    cache.set(game_id, json.dumps({
        "game_id": game_id,
        'last_player': player_code,
        'game_status': game_status
        }))

    winner = find_winner(game_status)

    return {'game_id': game_id, 'game_status': game_status, 'winner': winner} 


def find_winner(game_status):

    if( game_status[0][0] == game_status[0][1] == game_status[0][2] ):
        return game_status[0][0]

    if( game_status[1][0] == game_status[1][1] == game_status[1][2] ):
        return game_status[1][0]

    if( game_status[2][0] == game_status[2][1] == game_status[2][2] ):
        return game_status[2][0]

    if( game_status[0][0] == game_status[1][0] == game_status[2][0] ):
        return game_status[0][0]

    if( game_status[0][1] == game_status[1][1] == game_status[2][1] ):
        return game_status[0][1]

    if( game_status[0][1] == game_status[1][2] == game_status[2][2] ):
        return game_status[0][1]

    if( game_status[0][0] == game_status[1][1] == game_status[2][2] ):
        return game_status[0][0]

    if( game_status[0][2] == game_status[1][1] == game_status[2][0] ):
        return game_status[0][2]

    return 0
