## Requirements

```sh
docker
docker-compose 
```

## How To Run

```sh
docker-compose up
```

## How to run a game play sample

```sh
python3 test.py
```
