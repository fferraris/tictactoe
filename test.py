import requests,json

#player 1 winner in [0,2][1,1][2,0]

r = requests.post('http://localhost:5000/startgame', data = '')
game_id = ( r.json()['game_id'])

print('\n#############################################')
print('Player1 moves to 02')
print('#############################################')
print(requests.post('http://localhost:5000/move', data = {'game_id': game_id, 'player_code': 1, 'position': '02'}).text)
print('\n###### ')

print('\n#############################################')
print('Player1 tries to move again')
print('#############################################')
print(requests.post('http://localhost:5000/move', data = {'game_id': game_id, 'player_code': 1, 'position': '01'}).text)


print('\n#############################################')
print('Player1 tries to move again')
print('#############################################')
print(requests.post('http://localhost:5000/move', data = {'game_id': game_id, 'player_code': 1, 'position': '01'}).text)


print('\n#############################################')
print('Player2 moves on another player position')
print('#############################################')
print(requests.post('http://localhost:5000/move', data = {'game_id': game_id, 'player_code': 2, 'position': '02'}).text)


print('\n#############################################')
print('Player2 moves outside the board')
print('#############################################')
print(requests.post('http://localhost:5000/move', data = {'game_id': game_id, 'player_code': 2, 'position': '13'}).text)

print('\n#############################################')
print('Player2 moves to 02')
print('#############################################')
print(requests.post('http://localhost:5000/move', data = {'game_id': game_id, 'player_code': 2, 'position': '12'}).text)

print('\n#############################################')
print('Player1 moves to 11')
print('#############################################')
print(requests.post('http://localhost:5000/move', data = {'game_id': game_id, 'player_code': 1, 'position': '11'}).text)

print('\n#############################################')
print('Player2 moves to 22')
print('#############################################')
print(requests.post('http://localhost:5000/move', data = {'game_id': game_id, 'player_code': 2, 'position': '22'}).text)

print('\n#############################################')
print('Player1 moves to 20 and wins')
print('#############################################')
print(requests.post('http://localhost:5000/move', data = {'game_id': game_id, 'player_code': 1, 'position': '20'}).text)

print('\n#############################################')
print('Player2 moves to 10 and wins')
print('#############################################')
print(requests.post('http://localhost:5000/move', data = {'game_id': game_id, 'player_code': 2, 'position': '10'}).text)

